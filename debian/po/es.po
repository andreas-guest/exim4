# exim4  debconf translation to spanish
# Copyright (C) 2004-2007 Software in the Public Interest
# This file is distributed under the same license as the exim4 package.
#
# Changes:
# - Initial translation
#       Fco. Javier Sanchez Castelo <javicastelo@ono.com>, 2004
# - Revision:
#       Javier Fernandez-Sanguino <jfs@debian.org>, 2004
#       Fernando J. Rodriguez (Herr Groucho) <groucho@nys.com.ar>, 2004
# - Updates: 
#       Javier Fernandez-Sanguino <jfs@debian.org>, 2006-2007
#
#  Traductores, si no conoce el formato PO, merece la pena leer la 
#  documentacion de gettext, especialmente las secciones dedicadas a este
#  formato, por ejemplo ejecutando:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Equipo de traduccion al espanol, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traduccion de Debian al espanol
#   http://www.debian.org/intl/spanish/coordinacion
#   especialmente las notas de traduccion en
#   http://www.debian.org/intl/spanish/notas
#
# - La guia de traduccion de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Notas:
# - FQDN: Nombre completo del equipo (NO: nombre totalmente cualificado)
# Ver: Message-ID: <20060830223128.GO26167@javifsp.no-ip.org>
#
# - �smarthost�: no se traduce
#
msgid ""
msgstr ""
"Project-Id-Version: exim4 4.34-1\n"
"Report-Msgid-Bugs-To: pkg-exim4-maintainers@lists.alioth.debian.org\n"
"POT-Creation-Date: 2007-07-18 08:29+0200\n"
"PO-Revision-Date: 2007-07-14 12:35+0200\n"
"Last-Translator: Javier Fernandez-Sanguino <jfs@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: From dcotherhostnames fetchmail site FQDN blah mbox\n"
"X-POFile-SpellExtra: README conf Reply smarthost Return Maildir exim TCP\n"
"X-POFile-SpellExtra: usr template var mail config localhost postmaster\n"
"X-POFile-SpellExtra: Sender share SMTP Exim spool IPv DNS Path org dpkg\n"
"X-POFile-SpellExtra: acc To doc fqdn mailname root MX\n"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid "Remove undelivered messages in spool directory?"
msgstr ""
"�Eliminar los mensajes no entregados del directorio de la cola de correo?"

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"There are e-mail messages in the Exim spool directory /var/spool/exim4/"
"input/ which have not yet been delivered. Removing Exim will cause them to "
"remain undelivered until Exim is re-installed."
msgstr ""
"Hay mensajes en el directorio de la cola de correo de exim �/var/spool/exim4/"
"input/� que no se han entregado todav�a. Si desinstala Exim estos no podr�n "
"enviarse hasta que lo reinstale."

#. Type: boolean
#. Description
#: ../exim4-base.templates:1001
msgid ""
"If this option is not chosen, the spool directory is kept, allowing the "
"messages in the queue to be delivered at a later date after Exim is re-"
"installed."
msgstr ""
"Se mantendr� el directorio de colas si no elige esta opci�n, lo que "
"permitir� gestionar los mensajes encolados m�s adelante despu�s de "
"reinstalar Exim."

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid "Reconfigure exim4-config instead of this package"
msgstr "Reconfigure exim4-config en lugar de este paquete"

#. Type: error
#. Description
#: ../exim4-base.templates:2001 ../exim4-daemon-heavy.templates:1001
#: ../exim4-daemon-light.templates:1001 ../exim4.templates:1001
msgid ""
"Exim4 has its configuration factored out into a dedicated package, exim4-"
"config. To reconfigure Exim4, use 'dpkg-reconfigure exim4-config'."
msgstr ""
"Se ha movido la configuraci�n de exim4 a un paquete dedicado: �exim4-"
"config�. Si quiere reconfigurar Exim4 deber�a ejecutar �dpkg-reconfigure "
"exim4-config�."

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "internet site; mail is sent and received directly using SMTP"
msgstr "Internet site; el correo se env�a y recibe directamente usando SMTP"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; received via SMTP or fetchmail"
msgstr ""
"el correo se env�a mediante un �smarthost�; se recibe a trav�s de SMTP o "
"fetchmail"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "mail sent by smarthost; no local mail"
msgstr "el correo se env�a mediante un �smarthost�; sin correo local"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "local delivery only; not on a network"
msgstr "solamente entrega local; sin red"

#. Type: select
#. Choices
#. Translators beware! the following six strings form a single
#. Choices menu. - Every one of these strings has to fit in a standard
#. 80 characters console, as the fancy screen setup takes up some space
#. try to keep below ~71 characters.
#. DO NOT USE commas (,) in Choices translations otherwise
#. this will break the choices shown to users
#: ../exim4-config.templates:1001
msgid "no configuration at this time"
msgstr "sin configuraci�n de momento"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid "General type of mail configuration:"
msgstr "Tipo de configuraci�n general del correo:"

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"Please select the mail server configuration type that best meets your needs."
msgstr ""
"Seleccione el tipo de configuraci�n de servidor de correo que se ajuste "
"mejor a sus necesidades."

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"Systems with dynamic IP addresses, including dialup systems, should "
"generally be configured to send outgoing mail to another machine, called a "
"'smarthost' for delivery because many receiving systems on the Internet "
"block incoming mail from dynamic IP addresses as spam protection."
msgstr ""
"Los sistemas con direcciones IP din�micas, incluyendo los que utilicen "
"acceso telef�nico, deber�an configurarse por regla general para enviar el "
"correo a otro servidor llamado �smarthost� para que realice el reparto. Esto "
"es necesario porque muchos sistemas receptores de correo en Internet "
"bloquean el correo entrante que provenga de direcciones IP din�micas como "
"medida de protecci�n contra el correo basura."

#. Type: select
#. Description
#: ../exim4-config.templates:1002
msgid ""
"A system with a dynamic IP address can receive its own mail, or local "
"delivery can be disabled entirely (except mail for root and postmaster)."
msgstr ""
"Un sistema que utilice una direcci�n IP din�mica puede recibir su propio "
"correo, o puede deshabilitarse el env�o local por completo (salvo para el "
"correo para �root� y �postmaster�)."

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid "Really leave the mail system unconfigured?"
msgstr "�Realmente quiere dejar el sistema de correo sin configurar?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:2001
msgid ""
"Until the mail system is configured, it will be broken and cannot be used. "
"Configuration at a later time can be done either by hand or by running 'dpkg-"
"reconfigure exim4-config' as root."
msgstr ""
"Su sistema de correo estar� inutilizado y no podr� utilizarse hasta que lo "
"configure. Puede configurarlo m�s tarde, ya sea manualmente o ejecutando "
"�dpkg-reconfigure exim4-config� como administrador."

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid "System mail name:"
msgstr "Nombre del sistema de correo:"

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"The 'mail name' is the domain name used to 'qualify' mail addresses without "
"a domain name."
msgstr ""
"El nombre de correo local es el nombre del dominio utilizado para "
"�cualificar� las direcciones de correo que no tienen un nombre de dominio."

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"This name will also be used by other programs. It should be the single, "
"fully qualified domain name (FQDN)."
msgstr ""
"Este nombre se usar� por otros programas. Deber�a ser el nombre completo del "
"equipo (FQDN)."

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"Thus, if a mail address on the local host is foo@example.org, the correct "
"value for this option would be example.org."
msgstr ""
"Por ejemplo, si la direcci�n de correo en el sistema local es �blah@ejemplo."
"org�, el valor correcto para esta opci�n es �ejemplo.org�."

#. Type: string
#. Description
#: ../exim4-config.templates:3001
msgid ""
"This name won't appear on From: lines of outgoing messages if rewriting is "
"enabled."
msgstr ""
"Este nombre no aparecer� en la l�nea �From:� de los correos salientes si "
"est� activa la reescritura."

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid "Other destinations for which mail is accepted:"
msgstr "Otros destinos para los que se acepta el correo:"

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"machine should consider itself the final destination. These domains are "
"commonly called 'local domains'. The local hostname (${fqdn}) and "
"'localhost' are always added to the list given here."
msgstr ""
"Introduzca la lista de dominios de destinatarios para los que esta m�quina "
"deber�a considerarse a s� misma como destino final. Separe los dominios de "
"la lista con punto y coma. Estos dominios se llaman habitualmente �dominios "
"locales�. Siempre se a�ade el nombre del equipo (${fqdn}) y �localhost� a la "
"lista dada aqu�."

#. Type: string
#. Description
#: ../exim4-config.templates:4001
msgid ""
"By default all local domains will be treated identically. If both a.example "
"and b.example are local domains, acc@a.example and acc@b.example will be "
"delivered to the same final destination. If different domain names should be "
"treated differently, it is necessary to edit the config files afterwards."
msgstr ""
"Por omisi�n todos los dominios se tratar�n igual. Si tanto �a.ejemplo� como "
"�b.ejemplo� son dominios locales se enviar� al mismo destinatario correos a "
"�acc@a.ejemplo� y �acc@b.ejemplo�. Si quiere diferenciar entre distintos "
"nombres de dominio necesitar� editar los archivos de configuraci�n "
"posteriormente."

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Domains to relay mail for:"
msgstr "Dominios para los que se reenv�a correo:"

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid ""
"Please enter a semicolon-separated list of recipient domains for which this "
"system will relay mail, for example as a fallback MX or mail gateway. This "
"means that this system will accept mail for these domains from anywhere on "
"the Internet and deliver them according to local delivery rules."
msgstr ""
"Introduzca la lista de dominios destino para los que el sistema encaminar� "
"correo, actuando, por ejemplo, como pasarela de correo o respaldo MX. Separe "
"los elementos de la lista por punto y coma. Esto significa que el sistema "
"aceptar� correo para este dominios desde cualquier destino en Internet y los "
"gestionar� de acuerdo a las reglas locales de entrega."

#. Type: string
#. Description
#: ../exim4-config.templates:5001
msgid "Do not mention local domains here. Wildcards may be used."
msgstr "No mencione los dominios locales aqu�. Puede utilizar comodines."

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid "Machines to relay mail for:"
msgstr "M�quinas para las cuales reenviar correo:"

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"Please enter a semicolon-separated list of IP address ranges for which this "
"system will unconditionally relay mail, functioning as a smarthost."
msgstr ""
"Introduzca una lista de rangos de direcciones IP para los que el sistema "
"encaminar� correo de forma incondicional, actuando para ellos como un "
"�smarthost�. Separe las direcciones con punto y coma."

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"You should use the standard address/prefix format (e.g. 194.222.242.0/24 or "
"5f03:1200:836f::/48)."
msgstr ""
"Use el formato est�ndar direcci�n/longitud (por ejemplo 194.222.242.0/24 o "
"5f03:1200:836f::/48)."

#. Type: string
#. Description
#: ../exim4-config.templates:6001
msgid ""
"If this system should not be a smarthost for any other host, leave this list "
"blank."
msgstr ""
"Deje este valor en blanco si el sistema no debe comportarse como un "
"�smarthost� para ning�n otro sistema."

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid "Visible domain name for local users:"
msgstr "Nombre de dominio visible para usuarios locales:"

#. Type: string
#. Description
#: ../exim4-config.templates:7001
msgid ""
"The option to hide the local mail name in outgoing mail was enabled. It is "
"therefore necessary to specify the domain name this system should use for "
"the domain part of local users' sender addresses."
msgstr ""
"Se ha activado la opci�n para ocultar el nombre de correo local en el correo "
"saliente. Es necesario que especifique el nombre de dominio que el sistema "
"deber�a utilizar para la parte de dominio de las direcciones de correo "
"origen utilizadas para los usuarios locales."

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid "IP address or host name of the outgoing smarthost:"
msgstr "Direcci�n IP o nombre de equipo para el �smarthost� saliente:"

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"Please enter the IP address or the host name of a mail server that this "
"system should use as outgoing smarthost. If the smarthost only accepts your "
"mail on a port different from TCP/25, append two colons and the port number "
"(for example smarthost.example::587 or 192.168.254.254::2525). Colons in "
"IPv6 addresses need to be doubled."
msgstr ""
"Introduzca la direcci�n IP o el nombre de equipo del servidor de correo que "
"deber�a utilizarse como servidor saliente (�smarthost�). Si el servidor s�lo "
"acepta su correo en un puerto distinto al puerto TCP/25 deber� incluir el "
"n�mero de puerto separ�ndolo con dos signos de dos puntos (por ejemplo, "
"�smarthost.ejemplo::587 � 192.168.254.254::2525). Los dos puntos en "
"direcciones IPv6 deber�n escribirse dos veces."

#. Type: string
#. Description
#: ../exim4-config.templates:8001
msgid ""
"If the smarthost requires authentication, please refer to the Debian-"
"specific README files in /usr/share/doc/exim4-base for notes about setting "
"up SMTP authentication."
msgstr ""
"Si el �smarthost� requiere que el sistema se autentique deber� consultar "
"c�mo definir la configuraci�n de la autenticaci�n SMTP en los archivos "
"README espec�ficos de Debian en �/usr/share/doc/exim4-base/�."

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Root and postmaster mail recipient:"
msgstr "Destinatario del correo de �root� y �postmaster�:"

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Mail for the 'postmaster', 'root', and other system accounts needs to be "
"redirected to the user account of the actual system administrator."
msgstr ""
"Debe redirigir los correos a �postmaster�, �root� y otras cuentas del "
"sistema a una cuenta del usuario que corresponda a la del administrador del "
"sistema real."

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"If this value is left empty, such mail will be saved in /var/mail/mail, "
"which is not recommended."
msgstr ""
"Todo el correo de este tipo se almacenar� en /var/mail/mail si deja vac�o "
"este valor, lo que no est� recomendado."

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid ""
"Note that postmaster's mail should be read on the system to which it is "
"directed, rather than being forwarded elsewhere, so (at least one of) the "
"users listed here should not redirect their mail off this machine. A 'real-' "
"prefix can be used to force local delivery."
msgstr ""
"Tenga en cuenta que los mensajes dirigidos al administrador de correo "
"(�postmaster�) normalmente deber�an leerse en el sistema al que va dirigido, "
"en lugar de enviarse a otra parte. As� (al menos uno de) los usuarios que "
"escoja no deber�an reenviar su correo fuera de �sta m�quina. Puede utilizar "
"el prefijo �real-� para obligar a utilizar la entrega local."

#. Type: string
#. Description
#: ../exim4-config.templates:9001
msgid "Multiple user names need to be separated by spaces."
msgstr "Si introduce m�s de un nombre de usuario debe separarlos con espacios."

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid "IP-addresses to listen on for incoming SMTP connections:"
msgstr "Direcciones IP en las que recibir conexiones SMTP entrantes:"

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"Please enter a semicolon-separated list of IP addresses. The Exim SMTP "
"listener daemon will listen on all IP addresses listed here."
msgstr ""
"Introduzca una lista de direcciones IP separadas por punto y coma. El "
"demonio que acepta las conexiones entrantes SMTP de Exim escuchar� en todas "
"las direcciones IP aqu� listadas."

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"An empty value will cause Exim to listen for connections on all available "
"network interfaces."
msgstr ""
"Si deja este valor en blanco, Exim podr� recibir conexiones desde cualquier "
"interfaz de red disponible."

#. Type: string
#. Description
#: ../exim4-config.templates:10001
msgid ""
"If this system only receives mail directly from local services (and not from "
"other hosts), it is suggested to prohibit external connections to the local "
"Exim daemon. Such services include e-mail programs (MUAs) which talk to "
"localhost only as well as fetchmail. External connections are impossible "
"when 127.0.0.1 is entered here, as this will disable listening on public "
"network interfaces."
msgstr ""
"Es recomendable prohibir conexiones externas a Exim si este equipo s�lo "
"recibe correo directamente de servicios locales (y no de otros equipos). "
"Estos servicios incluyendo los programas cliente de correo (MUA) que s�lo "
"env�an correo a �localhost� as� como fetchmail. No podr�n realizarse "
"conexiones de forma externa si introduce aqu� el valor '127.0.0.1', de esta "
"forma desactivar� la escucha en las interfaces conectadas a redes p�blicas."

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid "Keep number of DNS-queries minimal (Dial-on-Demand)?"
msgstr "�Limitar el n�mero de consultas de DNS (marcaci�n bajo demanda)?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"In normal mode of operation Exim does DNS lookups at startup, and when "
"receiving or delivering messages. This is for logging purposes and allows "
"keeping down the number of hard-coded values in the configuration."
msgstr ""
"En el modo normal de operaci�n, Exim hace consultas de DNS al iniciar y "
"cuando recibe o entrega mensajes. Estas consultas se hacen para poder "
"mantener registros y para mantener peque�o el n�mero de valores fijos "
"grabados en el archivo de configuraci�n."

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"If this system does not have a DNS full service resolver available at all "
"times (for example if its Internet access is a dial-up line using dial-on-"
"demand), this might have unwanted consequences. For example, starting up "
"Exim or running the queue (even with no messages waiting) might trigger a "
"costly dial-up-event."
msgstr ""
"Si este sistema no tiene acceso permanente a servidores de nombres DNS (es "
"el caso cuando se usa marcaci�n bajo demanda o una l�nea de acceso "
"telef�nico) esto podr�a tener consecuencia no deseadas. Por ejemplo, se "
"podr�a llegar a intentar realizar una evento de conexi�n (que genere una "
"llamada costosa) al iniciar exim o procesar la cola del correo (incluso "
"aunque no haya mensajes en espera)."

#. Type: boolean
#. Description
#: ../exim4-config.templates:11001
msgid ""
"This option should be selected if this system is using Dial-on-Demand. If it "
"has always-on Internet access, this option should be disabled."
msgstr ""
"Deber�a seleccionar esta opci�n si su sistema utiliza marcaci�n bajo "
"demanda. No la habilite si su sistema est� permanentemente conectado a "
"Internet."

#. Type: title
#. Description
#: ../exim4-config.templates:12001
msgid "Mail Server configuration"
msgstr "Configuraci�n del servidor de correo:"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid "Split configuration into small files?"
msgstr "�Dividir la configuraci�n en peque�os ficheros?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"The Debian exim4 packages can either use 'unsplit configuration', a single "
"monolithic file (/etc/exim4/exim4.conf.template) or 'split configuration', "
"where the actual Exim configuration files are built from about 50 smaller "
"files in /etc/exim4/conf.d/."
msgstr ""
"Los paquetes Debian de exim4 pueden usar una �configuraci�n concentrada�, es "
"decir, un �nico fichero monol�tico (�/etc/exim4.conf.template�), o bien una "
"�configuraci�n segmentada� donde la configuraci�n de Exim se construye "
"utilizando cerca de 50 ficheros peque�os en �/etc/exim4/conf.d/�."

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"Unsplit configuration is better suited for large modifications and is "
"generally more stable, whereas split configuration offers a comfortable way "
"to make smaller modifications but is more fragile and might break if "
"modified carelessly."
msgstr ""
"La configuraci�n concentrada se adapta mejor a grandes modificaciones y es "
"generalmente m�s estable, mientras que la configuraci�n segmentada ofrece "
"una manera c�moda de hacer peque�as modificaciones pero es m�s fr�gil y "
"podr�a romperse si se modifica sin las debidas precauciones."

#. Type: boolean
#. Description
#: ../exim4-config.templates:13001
msgid ""
"A more detailed discussion of split and unsplit configuration can be found "
"in the Debian-specific README files in /usr/share/doc/exim4-base."
msgstr ""
"Encontrar� una discusi�n pormenorizada sobre la configuraci�n segmentada y "
"concentrada en los ficheros espec�ficos de Debian en �/usr/share/doc/exim4-"
"base/�."

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid "Hide local mail name in outgoing mail?"
msgstr "�Desea ocultar el nombre de correo local en los mensajes salientes?"

#. Type: boolean
#. Description
#: ../exim4-config.templates:14001
msgid ""
"The headers of outgoing mail can be rewritten to make it appear to have been "
"generated on a different system. If this option is chosen, '${mailname}', "
"'localhost' and '${dc_other_hostnames}' in From, Reply-To, Sender and Return-"
"Path are rewritten."
msgstr ""
"Las cabeceras de los mensajes salientes pueden reescribirse para que parezca "
"que se han generado en un sistema distinto. Si elige esta opci�n se "
"reescibir�n �${mailname}�, �localhost� y �${dc_other_hostnames}� en el "
"�From�, �Reply-To�, �Sender� y �Return-Path�."

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "mbox format in /var/mail/"
msgstr "formato mbox en �/var/mail�"

#. Type: select
#. Choices
#: ../exim4-config.templates:15001
msgid "Maildir format in home directory"
msgstr "formato �Maildir� en el directorio personal"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid "Delivery method for local mail:"
msgstr "Mecanismo de entrega para el correo local:"

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Exim is able to store locally delivered email in different formats. The most "
"commonly used ones are mbox and Maildir. mbox uses a single file for the "
"complete mail folder stored in /var/mail/. With Maildir format every single "
"message is stored in a separate file in ~/Maildir/."
msgstr ""
"Exim puede almacenar el correo entregado localmente en distintos formatos. "
"Los formatos m�s comunes son mbox y Maildir. El formato mbox utiliza un "
"�nico fichero para todo el buz�n de correo que se almacenan en �/var/mail�. "
"Con el formato Maildir cada uno de los mensajes se almacena en un fichero "
"distinto en �~/Maildir/�."

#. Type: select
#. Description
#: ../exim4-config.templates:15002
msgid ""
"Please note that most mail tools in Debian expect the local delivery method "
"to be mbox in their default."
msgstr ""
"Tenga en cuenta que la mayor�a de las herramientas de correo en Debian "
"esperan que el mecanismo de entrega local sea a ficheros mbox en su "
"configuraci�n por omisi�n."

#~ msgid "Move undelivered mails from exim 3 to exim4 spool?"
#~ msgstr ""
#~ "�Mover el correo a�n no entregado de exim 3 a la cola de correo de exim4?"

#~ msgid ""
#~ "There are some undelivered mails in exim 3 (or exim-tls 3) spool "
#~ "directory /var/spool/exim/input/."
#~ msgstr ""
#~ "Hay algunos mensajes de correo no entregados de exim 3 (o de exim-tls 3) "
#~ "en en el directorio de la cola de correo: �/var/spool/exim/input/�."

#~ msgid ""
#~ "Choosing this option will move these messages to exim4's spool (/var/"
#~ "spool/exim4/input/) where they will be handled by exim4."
#~ msgstr ""
#~ "Si elige esta opci�n se mover�n ahora a la cola de exim4 (�/var/spool/"
#~ "exim4/input/�) donde ser�n gestionados por exim4."

#~ msgid ""
#~ "This works only one-way: Exim4 can handle Exim 3 spool but not vice-"
#~ "versa. If you reject this option, you need to move the messages yourself "
#~ "or they will never be delivered."
#~ msgstr ""
#~ "Esto funciona solamente en un sentido, es decir, Exim4 puede gestionar la "
#~ "cola de Exim 3 pero no al contrario. Si rechaza esta opci�n, deber� mover "
#~ "los mensajes manualmente o nunca se enviar�n."

#~ msgid "Leaving this list blank will have Exim do no local deliveries."
#~ msgstr "Si deja esta lista en blanco entonces Exim no har� entrega local."

#~ msgid ""
#~ "This works only one-way: exim4 can handle exim(v3)'s spool but not vice-"
#~ "versa."
#~ msgstr ""
#~ "Tenga presente que esto funciona solamente en un sentido, es decir, exim4 "
#~ "puede gestionar la cola de exim(v3) pero no al contrario. "

#~ msgid ""
#~ "So it is only advisable to move the messages only if it is not planned to "
#~ "go back to Exim(v3). If a rollback might be necessary, it is a better "
#~ "idea to refrain from moving the messages now but moving them manually at "
#~ "a later time."
#~ msgstr ""
#~ "S�lo es recomendable mover los mensajes si no piensa volver a utilizar "
#~ "Exim(v3). Si puede necesitar hacer una marcha atr�s ser� mejor que no "
#~ "mueva los mensajes de momento y lo haga manualmente m�s adelante."

#~ msgid ""
#~ "Domains listed here need to be separated by semicolons. Wildcards may be "
#~ "used."
#~ msgstr ""
#~ "Separe los dominios introducidos aqu� con dos puntos. Puede usar "
#~ "comodines."

#~ msgid ""
#~ "Move the mails only if you don't plan to go back to exim(v3), otherwise "
#~ "the mail shouldn't be moved now but manually once you've converted your "
#~ "setup."
#~ msgstr ""
#~ "Mueva los mensajes de correo solamente si no tiene pensado volver a usar "
#~ "exim(v3). Si esto no es as�, no deber�a mover el correo ahora, sino "
#~ "manualmente una vez que haya adaptado su configuraci�n."

#~ msgid ""
#~ "If there are any more, enter them here, separated by semicolons. You may "
#~ "leave this blank if there are none."
#~ msgstr ""
#~ "Si hay algunos m�s, introd�zcalos aqu�, separados por dos puntos. Puede "
#~ "dejarlo en blanco si no hay ninguno."

#~ msgid ""
#~ "Please enter here the domains for which this system will relay mail, for "
#~ "example as a fallback MX or mail gateway."
#~ msgstr ""
#~ "Por favor introduzca aqu� los dominios a los que permite que reenv�en "
#~ "correo, por ejemplo como pasarela de correo o respaldo MX."

#~ msgid ""
#~ "Such domains are domains for which you are prepared to accept mail from "
#~ "anywhere on the Internet. Do not mention local domains here."
#~ msgstr ""
#~ "Estos dominios son aquellos para los que usted est� dispuesto a admitir "
#~ "correo desde cualquier parte de Internet. No indique aqu� dominios "
#~ "locales."

#~ msgid ""
#~ "Since you enabled hiding the local mailname in outgoing mail, you must "
#~ "specify the domain name to use for mail from local users; typically this "
#~ "is the machine on which you normally receive your mail."
#~ msgstr ""
#~ "Dado que ha activo la ocultaci�n del nombre de correo local en el correo "
#~ "saliente, debe especificar el nombre de dominio a usar para el correo de "
#~ "los usuarios locales. Habitualmente es el nombre de la m�quina en la que "
#~ "normalmente recibe su correo."

#~ msgid "Where will your users read their mail?"
#~ msgstr "�D�nde leer�n el correo sus usuarios?"

#~ msgid "Machine handling outgoing mail for this host (smarthost):"
#~ msgstr ""
#~ "M�quina que gestiona el correo saliente en este servidor (�smarthost�):"

#~ msgid "Enter the hostname of the machine to which outgoing mail is sent."
#~ msgstr ""
#~ "Introduzca el nombre del servidor al que se env�a el correo saliente."

#~ msgid ""
#~ "Mail for the \"postmaster\", \"root\", and other system accounts is "
#~ "usually redirected to the user account of the actual system "
#~ "administrator. If you leave this value empty, such mail will be saved in /"
#~ "var/mail/mail, which is not recommended. Note that postmaster's mail "
#~ "should be read on the system to which it is directed, rather than being "
#~ "forwarded elsewhere, so (at least one of) the users you choose should not "
#~ "redirect their mail off this machine. Use a \"real-\" prefix to force "
#~ "local delivery."
#~ msgstr ""
#~ "El correo para las cuentas del sistema �postmaster�, �root�, y algunas "
#~ "otras habitualmente se reenv�a a la cuenta de usuario del administrador "
#~ "real del sistema. Si deja este valor vac�o, este correo se almacenar� en "
#~ "�/var/mail/mail�, lo cual no es recomendable. Tenga en cuenta que el "
#~ "correo a �postmaster� deber�a leerse en el sistema al que se dirige, en "
#~ "lugar de reenviarse a otro lugar, de forma que (al menos uno de) estos "
#~ "usuarios no deber�an redirigirse fuera de este equipo. Utilice el prefijo "
#~ "�real-� para forzar el env�o local."

#~ msgid ""
#~ "Enable this feature if you are using Dial-on-Demand; otherwise, disable "
#~ "it."
#~ msgstr ""
#~ "Active esta funci�n si esta usando marcaci�n bajo demanda; en caso "
#~ "contrario, desact�vela."

#~ msgid "Select the mail server configuration type that best fits your needs."
#~ msgstr ""
#~ "Seleccione la configuraci�n de servidor de correo que se ajuste mejor a "
#~ "sus necesidades."

#~ msgid "If you are unsure then you should not use split configuration."
#~ msgstr ""
#~ "Si no est� seguro no deber�a usar la configuraci�n dividida en peque�os "
#~ "ficheros."

#~ msgid "manually convert from handcrafted Exim v3 configuration"
#~ msgstr ""
#~ "convierte manualmente a partir de la configuraci�n manual de exim v3"

#~ msgid "Configure Exim4 manually?"
#~ msgstr "�Configurar Exim4 manualmente?"

#~ msgid ""
#~ "You indicated that you have a handcrafted Exim 3 configuration. To "
#~ "convert this to Exim 4, you can use the exim_convert4r4(8) tool after the "
#~ "installation. Consult /usr/share/doc/exim4-base/examples/example.conf.gz "
#~ "and /usr/share/doc/exim4-base/README.Debian.gz!"
#~ msgstr ""
#~ "Usted indic� que tiene una configuraci�n manual de Exim 3. Para "
#~ "convertirla a Exim4, puede usar la herramienta exim_convert4r4(8) despu�s "
#~ "de la instalaci�n. �Consulte �/usr/share/doc/exim4-base/examples/example."
#~ "conf.gz� y �/usr/share/doc/exim4-base/README.Debian.gz�!"

#~ msgid ""
#~ "Until your mail system is configured, it will be broken and cannot be "
#~ "used."
#~ msgstr ""
#~ "Hasta que su sistema de correo no est� configurado, estar� inutilizado y "
#~ "no podr� usarse."

#~ msgid ""
#~ "Your \"mail name\" is the hostname portion of the address to be shown on "
#~ "outgoing news and mail messages (following the username and @ sign) "
#~ "unless hidden with rewriting."
#~ msgstr ""
#~ "Su �mail name� es el nombre de dominio en la direcci�n de correo que se "
#~ "mostrar� en los mensajes de noticias y de correo salientes (la que sigue "
#~ "al nombre de usuario tras el signo @), a no ser que est� oculta mediante "
#~ "reescritura."

#~ msgid ""
#~ "Please enter here the networks of local machines for which you accept to "
#~ "relay the mail."
#~ msgstr ""
#~ "Por favor introduzca aqu� las redes de m�quinas locales que permite que "
#~ "reenv�en correo."

#~ msgid ""
#~ "This should include a list of all machines that will use us as a "
#~ "smarthost."
#~ msgstr ""
#~ "Esto deber�a incluir la lista de todas las m�quinas que utilizar�n este "
#~ "sistema como �smarthost�."

#~ msgid ""
#~ "You need to double the colons in IPv6 addresses (e.g. "
#~ "5f03::1200::836f::::/48)"
#~ msgstr ""
#~ "Debe duplicar los dos puntos en direcciones IPv6 (por ejemplo "
#~ "5f03::1200::836f::::/48)"

#~ msgid ""
#~ "Enter a colon-separated list of IP-addresses to listen on.  You need to "
#~ "double the colons in IPv6 addresses (e.g. 5f03::1200::836f::::)."
#~ msgstr ""
#~ "Introduzca en una lista separada por dos puntos las direcciones IP en las "
#~ "que escuchar. Debe duplicar los dos puntos en direcciones IPv6 (por "
#~ "ejemplo 5f03::1200::836f::::)."

#~ msgid "Configuring Exim v4 (exim4-config)"
#~ msgstr "Configuraci�n de Exim versi�n 4 (exim4-config)"

#~ msgid ""
#~ "If you are configuring this system without local mail delivery this name "
#~ "won't appear on From: lines of mail, as rewriting is used."
#~ msgstr ""
#~ "Debido al uso de la reescritura de campos, este nombre no aparecer� en el "
#~ "campo �From:� de la cabecera del correo si no ha configurando env�o local "
#~ "de correo para este sistema."

#~ msgid ""
#~ "Obviously, any machines that use us as a smarthost have to be excluded "
#~ "from the relaying controls, as using us to relay mail for them is the "
#~ "whole point."
#~ msgstr ""
#~ "Obviamente, cualquier servidor que nos utilice como �smarthost� ha de ser "
#~ "exclu�do de los controles de reenv�o, ya que la funci�n primaria de "
#~ "esteservidor de correo es permitirles que estos lo reenvien desde aqu�."

#~ msgid ""
#~ "Mail for the \"postmaster\" and \"root\" accounts is usually redirected "
#~ "to one or more user accounts of the actual system administrators. The "
#~ "default is to set things up so that mail for \"postmaster\" and for "
#~ "various system accounts is redirected to \"root\", and mail for \"root\" "
#~ "is redirected to a real user.  This can be changed by editing /etc/"
#~ "aliases."
#~ msgstr ""
#~ "El correo de las cuentas de �postmaster� (administrador de correo) y "
#~ "�root� (administrador) habitualmente se redirige a una o m�s cuentas de "
#~ "usuario de los usuarios que son realmente administradores del sistema. El "
#~ "valor por omisi�n es que el correo para �postmaster� y el de varias "
#~ "cuentas del sistema se reenv�a a �root�, y el correo de �root� se reenv�a "
#~ "a un usuario real. Esto se puede cambiar editando �/etc/aliases�."

#~ msgid ""
#~ "System administrator mail goes to which user accounts? Enter one or more "
#~ "usernames separated by spaces or commas.  Enter \"none\" if you do not "
#~ "want to redirect the mail. - NB this is strongly discouraged. Exim cannot "
#~ "run deliveries as root and will save the mail to /var/mail/mail  Also, "
#~ "note that usernames should be lowercase!"
#~ msgstr ""
#~ "�A qu� cuentas de usuario ir� el correo del administrador del sistema? "
#~ "Introduzca uno o m�s nombres de usuario separados por espacios o comas. "
#~ "Escriba �none� si no desea redirigir el correo. No se recomienda jam�s "
#~ "hacer �sto. Exim no puede efectuar entregas de correo como root y "
#~ "guardar� el correo en �/var/mail/mail�. Asimismo, �tenga en cuenta que "
#~ "los nombres de usuario est�n en min�scula!"
